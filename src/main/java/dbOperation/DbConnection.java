package dbOperation;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;

public class DbConnection {

    static public MongoClient mongoClient;

    static {
        try{

            // To directly connect to the default server localhost on port 27017
            //mongoClient = MongoClients.create();
            // Use a Connection String
            mongoClient = MongoClients.create("mongodb://localhost");



            ///com.mongodb.MongoClient mongo = new com.mongodb.MongoClient( "localhost" , 27017 );
        }catch(Exception e){
            System.out.println(e.getCause() + "\n" + e.toString());
        }
    }



    public static MongoClient getMongoClient(){
        return mongoClient;
    }
    public static void closeConnection(){
        mongoClient.close();
    }

}
