package dbOperation;

import com.mongodb.Block;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import model.Course;
import model.Student;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import resource.CourseValidation;
import resource.EnrollmentChecking;
import resource.UserValidation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;
import static dbOperation.DbConnection.mongoClient;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class StudentOperation {
    static MongoDatabase database;
    private Student s;

    public StudentOperation(){
        database = mongoClient.getDatabase("enrollDb");
        this.s = s;


    }



    public void registerStudent(Student s, final CountDownLatch latch){
        MongoCollection<Document> collection = database.getCollection("Student");

        //System.out.println("running");
        Document doc = new Document("id", s.getStudentId())
                .append("userName", s.getUsername())
                .append("password", s.getPassword())
                .append("fname", s.getfName() )
                .append("lname", s.getlName())
                .append("email", s.getEmail())
                .append("phone", s.getPhone());

        collection.insertOne(doc, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("Registration Successful...!");

                latch.countDown();
            }
        });

    }


   public boolean verifyStudent(final Student s, final CountDownLatch latch,final UserValidation log) throws InterruptedException {
        MongoCollection<Document> students = database.getCollection("Student");
        final int state;
        final List<Document> myList = new ArrayList<Document>();
        students.find().into(myList,
                new SingleResultCallback<List<Document>>() {
                    @Override
                    public void onResult(final List<Document> result, final Throwable t) {
                        
                        for (int i = 0; i < myList.size(); i++) {
                            
                            Document document = myList.get(i);
                            
                            if((document.get("userName")).equals(s.getUsername()) && (document.get("password")).equals(s.getPassword())){

                                System.out.println("Login Successful..");
                                log.setValid(true,document);
                                break;
                            }


                        }
                        latch.countDown();
                    }

                }
        );

        return true;
    }



    public void checkUserName(final String user, final CountDownLatch latch, final UserValidation uv) {
        // create codec registry for POJOs
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClients.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // get handle to "enrollDb" database
        MongoDatabase database1 = database.withCodecRegistry(pojoCodecRegistry);

        // get a handle to the "Course" collection
        final MongoCollection<Student> collection = database1.getCollection("Student", Student.class);

        SingleResultCallback<Student> printCallback = new SingleResultCallback<Student>() {
            @Override
            public void onResult(final Student student, final Throwable t) {
                //System.out.println(course);

                if(student != null){
                    //System.out.println("exist");
                    uv.setUserExist(true);
                }
                latch.countDown();
            }
        };


        collection.find(eq("userName", user)).first(printCallback);
        //collection = collection.withCodecRegistry(pojoCodecRegistry);

    }


    public void viewStudent(Document d){
        System.out.println("First Name : "+d.get("fname"));

        System.out.println("Last Name  : "+d.get("lname"));

        System.out.println("Email      : "+d.get("email"));

        System.out.println("Phone      : "+d.get("phone"));

    }


    public static void updateStudent(final Document d, Student s,final CountDownLatch latch) throws InterruptedException {
        MongoCollection<Document> students = database.getCollection("Student");

        final Document doc = d;

        //update a student
        students.updateOne(
                eq("_id", new ObjectId(String.valueOf(d.get("_id")))),
                combine(set("password", s.getPassword()),
                        set("fname", s.getfName() ),
                        set("lname", s.getlName()),
                        set("email", s.getEmail()),
                        set("phone", s.getPhone()),
                        currentDate("lastModified")),
                new SingleResultCallback<UpdateResult>() {
                    @Override
                    public void onResult(final UpdateResult result, final Throwable t) {
                        //System.out.println(result.getModifiedCount());
                        System.out.println("updated Successfully...!");
                        System.out.println("\n");
                        latch.countDown();

                    }
                });

        //set("userName", s.getUsername()),
        
    }


    public void validateCourse(String cid, final CountDownLatch latch, final CourseValidation cv){

        // create codec registry for POJOs
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClients.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // get handle to "enrollDb" database
        MongoDatabase database1 = database.withCodecRegistry(pojoCodecRegistry);

        // get a handle to the "Course" collection
        final MongoCollection<Course> collection = database1.getCollection("Course", Course.class);

        SingleResultCallback<Course> printCallback = new SingleResultCallback<Course>() {
            @Override
            public void onResult(final Course course, final Throwable t) {
                //System.out.println(course);

                if(course == null){
                    cv.setValid(false);
                }
                latch.countDown();
            }
        };


        collection.find(eq("courseId", cid)).first(printCallback);
        //collection = collection.withCodecRegistry(pojoCodecRegistry);

    }


    public void enrollCourses(final String cid, final Document d, final CountDownLatch latch) throws InterruptedException {

        MongoCollection<Document> collection = database.getCollection("Enrollment");

        Document doc = new Document("courseId", cid)
                .append("StudentId", d.get("_id"))
                .append("grade", 0)
                .append("userName", d.get("userName"));



        //doneLatch.countDown();
        collection.insertOne(doc, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("Successfully enrolled to "+cid);
                latch.countDown();
            }
        });
    }



    public void viewEnrolledCourses(final Document d,final CountDownLatch latch){

        MongoCollection<Document> collection = database.getCollection("Enrollment");

        Block<Document> printDocumentBlock = new Block<Document>() {
            @Override
            public void apply(final Document document) {
                //System.out.println(document.toJson());
                System.out.println(document.get("courseId")+ " :   "+document.get("grade"));
            }
        };
        SingleResultCallback<Void> callbackWhenFinished = new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                //System.out.println("Operation Finished!");
                latch.countDown();
            }
        };

        collection.find(eq("StudentId", d.get("_id"))).forEach(printDocumentBlock, callbackWhenFinished);
        //collection.find(eq("_id", d.get("_id"))).projection(excludeId()).forEach(printDocumentBlock, callbackWhenFinished);

    }


    public void unenrollFromCourse(String cid,Document d,final CountDownLatch latch, final EnrollmentChecking ec){
        MongoCollection<Document> collection = database.getCollection("Enrollment");

        collection.deleteOne(and(eq("courseId", cid),eq("StudentId", d.get("_id")) ), new SingleResultCallback<DeleteResult>() {
            @Override
            public void onResult(final DeleteResult result, final Throwable t) {
                //System.out.println(result.getDeletedCount());
                if(result.getDeletedCount() > 0){
                    ec.setEnroll(false);
                }

                latch.countDown();
            }


        });

    }

    /* public void checkUserName(final String user, final CountDownLatch latch2, UserValidation uv) {
        final MongoCollection<Document> students = database.getCollection("Student");

        students.createIndex(Indexes.text("userName"), new SingleResultCallback<String>() {
            @Override
            public void onResult(final String result, final Throwable t) {
                System.out.println("Operation Finished!");

                students.count(Filters.text(user), new SingleResultCallback<Long>() {
                    @Override
                    public void onResult(final Long count, final Throwable t) {
                        System.out.println("Text search matches: " + count);

                        latch2.countDown();
                    }
                });
            }
        });


        }

    */

}


