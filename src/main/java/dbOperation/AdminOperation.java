package dbOperation;

import com.mongodb.Block;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import model.Admin;
import model.Course;
import model.Enrollment;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import resource.EnrollmentChecking;
import resource.UserValidation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static dbOperation.DbConnection.mongoClient;
import static dbOperation.DbConnection.mongoClient;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class AdminOperation {
    static MongoDatabase database;
    static MongoDatabase database1;

    public AdminOperation(){
        database = mongoClient.getDatabase("enrollDb");

        // create codec registry for POJOs
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClients.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        // get handle to "enrollDb" database
         this.database1 = database.withCodecRegistry(pojoCodecRegistry);

    }

    static SingleResultCallback<Document> callbackPrintDocuments = new SingleResultCallback<Document>() {
        @Override
        public void onResult(final Document document, final Throwable t) {
            System.out.println(document.toJson());
        }
    };




    public void viewAllCourses(final CountDownLatch latch) throws InterruptedException {
        SingleResultCallback<Void> callbackWhenFinished = new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                //System.out.println("Operation Finished!");
                latch.countDown();
            }
        };

         Block<Document> printDocumentBlock = new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document.toJson());
            }
        };


        MongoCollection<Document> Courses = database.getCollection("Course");
        Courses.find().projection(excludeId()).forEach(printDocumentBlock, callbackWhenFinished);
        //latch.countDown();

    }


    public void checking(final String id, final Document d,final EnrollmentChecking ec,final CountDownLatch latch) throws InterruptedException {

        final MongoCollection<Document> collection = database.getCollection("Enrollment");
        final List<Document> myList2 = new ArrayList<Document>();
        collection.find().into(myList2,
                new SingleResultCallback<List<Document>>() {
                    @Override
                    public void onResult(final List<Document> result, final Throwable t) {

                        for (int i = 0; i < myList2.size(); i++) {


                            Document document = myList2.get(i);
                            if((document.get("courseId")).equals(id) && (document.get("StudentId")).equals(d.get("_id"))){
                                ec.setEnroll(false);
                               // latch.countDown();
                                break;

                            }

                        }
                    latch.countDown();
                    }

                }
        );


    }


    public void validateAdmin(Admin ad, final CountDownLatch latch, final UserValidation uv){


        // get a handle to the "Admin" collection
        final MongoCollection<Admin> collection = database1.getCollection("Admin", Admin.class);

        SingleResultCallback<Admin> printCallback = new SingleResultCallback<Admin>() {
            @Override
            public void onResult(final Admin admin, final Throwable t) {
                //System.out.println(admin);

                if(admin != null)
                    uv.setUserExist(true);

                latch.countDown();
            }
        };
        collection.find(and(eq("userName", ad.getUser()),eq("password", ad.getPw()))).first(printCallback);

    }


    public void addCourse(Course co,final CountDownLatch latch){

        MongoCollection<Course> collection = database1.getCollection("Course", Course.class);

        Course nwcourse = co;
        collection.insertOne(nwcourse, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("\n!....Inserted successfully....!");
                latch.countDown();
            }
        });

    }


    public void updateCourse(Course co,final CountDownLatch latch){
        MongoCollection<Course> collection = database1.getCollection("Course", Course.class);

        SingleResultCallback<UpdateResult> printModifiedCount = new SingleResultCallback<UpdateResult>() {
            @Override
            public void onResult(final UpdateResult result, final Throwable t) {
                System.out.println(result.getModifiedCount());

                if(result.getModifiedCount()>0){
                    System.out.println("\n!....Updated successfully....!");
                }
                else
                    System.out.println("\n!....update fail, incorrect Course Id....!\n");
                latch.countDown();
            }
        };
        collection.updateOne(eq("courseId", co.getCourseId()), combine(set("courseName", co.getCourseName()), set("profName", co.getProfName()),set("credit", co.getCredit()),set("location", co.getLocation())), printModifiedCount);
    }

    public void setCourseGrade(Enrollment en,final CountDownLatch latch){
        MongoCollection<Enrollment> collection = database1.getCollection("Enrollment", Enrollment.class);

        SingleResultCallback<UpdateResult> printModifiedCount = new SingleResultCallback<UpdateResult>() {
            @Override
            public void onResult(final UpdateResult result, final Throwable t) {
                System.out.println(result.getModifiedCount());

                if(result.getModifiedCount()>0)
                    System.out.println("\n!....added student's course grade successfully....!\n");

                else
                    System.out.println("\n!....update fail, incorrect Course Id or user name....!\n");

                 latch.countDown();
            }
        };
        collection.updateOne(and(eq("courseId", en.getCourse_id()),eq("userName", en.getUserName())), set("grade", en.getGrade()), printModifiedCount);

    }


}
