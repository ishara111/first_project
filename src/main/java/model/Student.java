package model;

public class Student {

    private static int studentId;
    private String username;
    private String password;
    private String fName;
    private String lName;
    private String email;
    private String phone;

    public Student(){ }

    public Student(String fName, String lName, String username, String password, String email, String phone){
       //this.studentId = studentId;
        studentId += studentId;
       this.username = username;
       this.password = password;
       this.fName = fName;
       this.lName = lName;
       this.email = email;
       this.phone = phone;

    }

    public Student( String username, String password){
        this.username = username;
        this.password = password;

    }

    public Student(String fName, String lName, String password, String email, String phone) {
        this.password = password;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phone = phone;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
