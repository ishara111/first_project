package model;

public class Enrollment {
    private double grade;
    private String userName;
    private String course_id;

    public Enrollment(){

    }

    public Enrollment(String course_id,String userName, double grade){
        this.course_id = course_id;
        this.grade = grade;
        this.userName = userName;
    }


    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }
}
