package model;

import org.bson.types.ObjectId;

public class Course {

    private ObjectId oid;
    private String courseId;
    private String courseName;
    private String profName;
    private String location;
    private double credit;
    //private String time;

    public Course(){ }

    public Course(String courseId, String courseName, String profName, String location, double credit){
        //this.oid = new ObjectId();
        this.courseId = courseId;
        this.courseName = courseName;
        this.profName = profName;
        this.location = location;
        this.credit = credit;
    }

    public ObjectId getOid() {
        return oid;
    }

    public void setOid(ObjectId oid) {
        this.oid = oid;
    }

    public String getProfName() {
        return profName;
    }

    public void setProfName(String profName) {
        this.profName = profName;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }
}
