package model;

public class Admin {
    private String user;
    private String pw;

    public Admin(){

    }

    public Admin(String user,String pw){
        this.user = user;
        this.pw =pw;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
}
