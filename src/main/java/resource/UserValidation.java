package resource;

import org.bson.Document;

public class UserValidation {
    boolean logged = false;
    boolean userExist = false;
    Document d;

    public Document getD() {
        return d;
    }

    public boolean isValid() {
        return logged;
    }

    public void setValid(boolean logged, Document d) {
        this.logged = logged;
        this.d = d;
    }

    public boolean isUserExist() {
        return userExist;
    }

    public void setUserExist(boolean userExist) {
        this.userExist = userExist;
    }
}
