package resource;

import model.Admin;
import model.Course;
import model.Enrollment;

import java.util.Scanner;

public class CourseDetailsResource {
    Scanner sc = new Scanner(System.in);

    public Admin validateAdmin(){
        System.out.println("Enter user name: ");
        String uname = sc.next();
        System.out.println("Enter password : ");
        String pw = sc.next();

        Admin ad = new Admin(uname,pw);
        return ad;

    }

    public Course addCourse(){

        System.out.println("Enter Course Id:");
        String cid = sc.next();
        sc.nextLine();

        System.out.println("Enter Course Name:");
        String course_name = sc.nextLine();

        System.out.println("Enter Professor Name:");
        String prof_name = sc.nextLine();

        System.out.println("Enter Location:");
        String location = sc.nextLine();

        System.out.println("Enter Credits:");
        double credit = sc.nextDouble();

        Course c = new Course(cid,course_name,prof_name,location,credit);

        return c;


    }


    public Course updateCourse() throws InterruptedException {

        System.out.println("Enter Course Id:");
        String cid = sc.next();
        sc.nextLine();

        System.out.println("Update the fields :");
        System.out.println("Enter Course Name:");
        String course_name = sc.nextLine();

        System.out.println("Enter Professor Name:");
        String prof_name = sc.nextLine();

        System.out.println("Enter Location:");
        String location = sc.nextLine();

        System.out.println("Enter Credits:");
        double credit = sc.nextDouble();

        Course c = new Course(cid,course_name,prof_name,location,credit);

        return c;

    }

    public Enrollment addGrade(){
        System.out.println("Enter Course Id:");
        String cid = sc.next();

        System.out.println("Enter student user name:");
        String user = sc.next();

        System.out.println("Enter Grade:");
        double grade = sc.nextDouble();

        Enrollment en = new Enrollment(cid,user,grade);

        return en;

    }

}
