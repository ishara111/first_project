package resource;

import dbOperation.StudentOperation;
import model.Student;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

public class StudentDetailsResource {

    public static Student regStudent() throws InterruptedException {
        Scanner sc= new Scanner(System.in);
        StudentOperation so = new StudentOperation();


        System.out.println("Enter First Name:");
        String fname = sc.next();

        System.out.println("Enter Last Name:");
        String lname = sc.next();

        String user_name;
        A:
        while(true){
            System.out.println("Enter User Name:");
            user_name = sc.next();
            //CountDownLatch latch1 = new CountDownLatch(1);
            UserValidation uv= new UserValidation();
            CountDownLatch latch2 = new CountDownLatch(1);
            //so.createIndex(latch1);
            //latch1.await();
            so.checkUserName(user_name,latch2,uv);
            latch2.await();

            if(uv.isUserExist()){
                System.out.println("User name already exists, try again");
                continue A;
            }
            else{
                break;
            }
        }


        System.out.println("Enter Password:");
        String pw = sc.next();

        System.out.println("Enter Email:");
        String em = sc.next();

        System.out.println("Enter phone:");
        String ph = sc.next();

        Student s= new Student(fname,lname,user_name,pw,em,ph);


        return s;
    }

    public static Student loginStudent() throws InterruptedException {
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter UserName:");
        String user_name = sc.next();
        System.out.println("Enter password:");
        String pw = sc.next();

        Student s= new Student(user_name,pw);

        return s;
    }

    public static Student updateStudents() throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First Name:");
        String fname = sc.next();

        System.out.println("Enter Last Name:");
        String lname = sc.next();

        //System.out.println("Enter User Name:");
        //String user_name = sc.next();

        System.out.println("Enter Password:");
        String pw = sc.next();

        System.out.println("Enter Email:");
        String em = sc.next();

        System.out.println("Enter phone:");
        String ph = sc.next();

        Student s= new Student(fname,lname,pw,em,ph);


        return s;
    }

}
