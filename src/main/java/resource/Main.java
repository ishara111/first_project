package resource;

import dbOperation.AdminOperation;
import dbOperation.StudentOperation;
import model.Admin;
import model.Course;
import model.Enrollment;
import model.Student;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

public class Main {


    public static void main(String[] args) throws InterruptedException {
        Scanner sc= new Scanner(System.in);
        StudentOperation so = new StudentOperation();
        AdminOperation ao = new AdminOperation();
        UserValidation log = new UserValidation();
        EnrollmentChecking ec = new EnrollmentChecking();
        StudentDetailsResource sdr = new StudentDetailsResource();
        CourseDetailsResource cdr = new CourseDetailsResource();


        System.out.println("Enter the mode:");
        System.out.println("Student ----> 1");
        System.out.println("Admin ----> 2");

        int respo=  sc.nextInt();

        if(respo ==1){
            int op;
            int op2;
            B:
            while(true){
                System.out.println("Select one option: ");
                System.out.println("  Register ---> 1");
                System.out.println("  Login --->2 ");
                System.out.println("  Exit --->3 ");

                op = sc.nextInt();
                CountDownLatch latch = new CountDownLatch(1);
                switch(op){
                    case 1: {

                        Student s = sdr.regStudent();
                        so.registerStudent(s,latch);
                        latch.await();
                        continue;
                    }
                    case 2: {
                        //CountDownLatch latch = new CountDownLatch(1);
                        Student st= sdr.loginStudent();
                        so.verifyStudent(st,latch,log);
                        latch.await();

                        if(log.isValid()){
                            A:
                            while(true){
                                System.out.println("Select one option: ");
                                System.out.println("  View---> 1");
                                System.out.println("  Update ---> 2");
                                System.out.println("  Enroll ---> 3");
                                System.out.println("  Unenroll ---> 4");
                                System.out.println("  Exit ---> 5");
                                op2 = sc.nextInt();
                                CountDownLatch latch2 = new CountDownLatch(1);

                                switch(op2){
                                    case 1: {
                                        int op5;
                                        System.out.println("to View Account details ----> 1 ");
                                        System.out.println("to View Enrolled course details ----> 2 ");

                                        op5 = sc.nextInt();

                                        switch(op5){
                                            case 1: {
                                                System.out.println("--------------------------------------");
                                                so.viewStudent(log.getD());
                                                System.out.println("--------------------------------------");

                                                System.out.println("\nto View Enrolled course details ----> 2 ");
                                                System.out.println("to previous menu ----> 3 ");
                                                op5 = sc.nextInt();
                                                if(op5 != 2) continue A;
                                            }
                                            case 2:{
                                                System.out.println("-------------------------------------");
                                                so.viewEnrolledCourses(log.getD(),latch2);
                                                latch2.await();
                                                System.out.println("-------------------------------------\n");
                                                continue A;
                                            }

                                            default: continue A;

                                        }

                                    }
                                    case 2: {
                                       ;
                                        Student s = sdr.updateStudents();
                                        so.updateStudent(log.getD(),s,latch2);
                                        latch2.await();
                                        continue B;
                                    }
                                    case 3:{
                                        CountDownLatch latch4 = new CountDownLatch(1);

                                        CountDownLatch latch5 = new CountDownLatch(1);
                                        System.out.println("------------------------------------------------------------------------------------------------------------------------");
                                        ao.viewAllCourses(latch2);
                                        latch2.await();
                                        System.out.println("------------------------------------------------------------------------------------------------------------------------");

                                        int op4;
                                        D:
                                        while(true){
                                            CourseValidation cv = new CourseValidation();
                                            CountDownLatch latch3 = new CountDownLatch(1);
                                            System.out.println("\nEnter Course Id:");

                                            String courseId = sc.next();

                                            so.validateCourse(courseId,latch3,cv);
                                            latch3.await();

                                            if(!cv.isValid()){
                                                System.out.println("Course Id is incorrect.. ");
                                                System.out.println("to Re-Enter course Id ----> 1 ");
                                                System.out.println("Go to previous menu   ----> 2 ");
                                                op4 = sc.nextInt();

                                                switch(op4){
                                                    case 1: continue D;
                                                    case 2: continue A;

                                                }
                                            }

                                            else{
                                                ao.checking(courseId,log.getD(),ec,latch5);
                                                latch5.await();
                                                if(ec.isEnroll()){
                                                    so.enrollCourses(courseId, log.getD(),latch4);
                                                    latch4.await();
                                                }
                                                else{
                                                    System.out.println("\nYou have already enrolled to "+courseId);
                                                    //latch4.countDown();
                                                    //latch4.await();
                                                }

                                                continue A;

                                            }



                                        }

                                    }
                                    case 4: {
                                        //CountDownLatch latch3 = new CountDownLatch(1);
                                        EnrollmentChecking ec2 = new EnrollmentChecking();
                                        System.out.println("Enrolled courses :");
                                        System.out.println("----------------------------------");
                                        System.out.println("Course   Grade");
                                        so.viewEnrolledCourses(log.getD(),latch2);
                                        latch2.await();
                                        System.out.println("----------------------------------");


                                        int op3;
                                        C:
                                        while(true){
                                            CountDownLatch latch3 = new CountDownLatch(1);
                                            System.out.println("Enter the Course Id:");
                                            String courseId = sc.next();

                                            so.unenrollFromCourse(courseId, log.getD(),latch3,ec2);
                                            latch3.await();

                                            if(ec2.isEnroll()){
                                                System.out.println("Course Id is incorrect. ");
                                                System.out.println("to Re-Enter course Id ----> 1 ");
                                                System.out.println("Go to previous menu   ----> 2 ");
                                                op3 = sc.nextInt();

                                                switch(op3){
                                                    case 1: continue C;
                                                    case 2: continue A;
                                                    default: continue A;
                                                }

                                            }

                                            else{
                                                System.out.println("Successfully unenrolled from course "+ courseId);
                                                continue A;
                                            }

                                        }


                                    }
                                    case 5: {
                                        System.exit(0);
                                    }
                                }
                            }
                        }
                        else{
                            System.out.println("Wrong user name or password");
                            continue B;
                        }

                    }
                    //break;
                    case 3: System.exit(0);
                    break;

                    default: {
                        System.out.println("input error !");
                        continue B;
                    }
                }
            }

        }

        else if(respo ==2){
            int op;
            int op2;

            A:
            while(true){
                Admin ad = cdr.validateAdmin();
                CountDownLatch latch = new CountDownLatch(1);
                UserValidation uv = new UserValidation();

                ao.validateAdmin(ad,latch,uv);
                latch.await();
                if(uv.isUserExist()){
                    break;
                }
                else{
                    System.out.println("details that you've entered are incorrect.");
                    System.out.println("  Re-enter user name & password -----> 1");
                    System.out.println("  Exit                          -----> 2 ");

                    op2 = sc.nextInt();

                    switch(op2){
                        case 1: continue A;
                        case 2:
                        default: System.exit(0);
                    }
                }

            }

            B:
            while(true){
                System.out.println("Select one option: ");
                System.out.println("  Add Courses    -----> 1");
                System.out.println("  Update Courses -----> 2 ");
                System.out.println("  View all courses ---> 3 ");
                System.out.println("  Set Student Grade --> 4 ");
                System.out.println("  Exit     -----------> 5 ");

                op = sc.nextInt();
                CountDownLatch latch = new CountDownLatch(1);
                switch (op){
                    case 1:{

                        Course c = cdr.addCourse();
                        ao.addCourse(c,latch);
                        latch.await();
                        continue B;
                    }

                    case 2:{
                        Course c= cdr.updateCourse();
                        ao.updateCourse(c,latch);
                        latch.await();
                        continue B;
                    }

                    case 3:{
                        System.out.println("------------------------------------------------------------------------------------------------------------------------");
                        ao.viewAllCourses(latch);
                        latch.await();
                        System.out.println("------------------------------------------------------------------------------------------------------------------------\n");
                        continue B;
                    }

                    case 4:{
                        Enrollment en = cdr.addGrade();
                        ao.setCourseGrade(en,latch);
                        latch.await();
                        continue B;
                    }

                    case 5:
                        System.exit(0);
                        break;

                    default:
                        continue B;
                }

            }

        }


        // Main thread has started
        /*System.out.println(Thread.currentThread().getName() +
                " has finished");
         */
    }



}
